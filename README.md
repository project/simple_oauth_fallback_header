# Simple OAuth: Fallback Header

This module allows the use of `X-OAuth-Authorization` HTTP header for sending `Bearer` access tokens to authenticate via [Simple OAuth](https://www.drupal.org/project/simple_oauth). The alternative header name may help using the basic HTTP authorization in combination with OAuth, eliminating headers conflict.

## Usage

The module has zero configuration and works as plug & play, so you have to simply install it.

### Configuration

If, for some reason, you're willing to rename the `X-OAuth-Authorization` header, use `settings.php` to do that:

```php
$settings['simple_oauth_fallback_header'] = 'X-My-Api-Auth';
```

Tune `settings.php` a little more if you want to allow sending an access token in the `access_token` GET parameter:

```php
/**
 * Allow access token to be sent in a GET query `/api?access_token=XXX`.
 *
 * @link https://tools.ietf.org/html/rfc6750#section-2.3
 */
$settings['simple_oauth_allow_get_query'] = TRUE;
```

#### Server-side HTTP authorization

A [service provider](https://www.drupal.org/docs/8/api/services-and-dependency-injection/altering-existing-services-providing-dynamic-services) needs to be implemented in order to work seamlessly with server-side basic/digest HTTP authorization.

```php
namespace Drupal\my_module;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;

/**
 * Module's service provider.
 */
class MyModuleServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    // Remove `basic_auth_swap` HTTP middleware provided by the `simple_oauth`.
    // It needed because a site may have server-based HTTP authentication
    // but that middleware converts `PHP_AUTH_USER` and `PHP_AUTH_PW` to
    // the request's `client_id` and `client_secret` and this breaks the
    // OAuth2 token creation.
    $container->removeDefinition('simple_oauth.http_middleware.basic_auth_swap');
  }

}
```

After that we're safe to use our custom header for OAuth authorization in combination with `Authorization` header for basic/digest HTTP authorization.

## Under the hood

The description of a logic the module provides might be relevant for someone, especially because of a request object mutation. Let's have a look.

### Prioritization

- The `X-OAuth-Authorization` header has a higher priority than `Authorization` header and GET query.
- The `access_token` GET query (if enabled) has a higher priority over standard `Authorization` header.

### Request mutation

This module checks whether the `X-OAuth-Authorization` header or `access_token` GET query meets in a request and places the value from one of them (based on prioritization) to the `Authorization` header. This means that **previously stored value in the `Authorization` header will be lost**.
