<?php

namespace Drupal\Tests\simple_oauth_fallback_header\Functional;

use Drupal\Core\Url;
use Drupal\Tests\simple_oauth\Functional\TokenBearerFunctionalTestBase;

/**
 * Tests the `simple_oauth_fallback_header` module.
 *
 * @group simple_oauth_fallback_header
 */
class SimpleOauthFallbackHeaderTest extends TokenBearerFunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'simple_oauth_fallback_header_test',
  ];

  /**
   * Tests that an alternative header for Bearer challenge can be used.
   */
  public function test(): void {
    $protected_route = Url::fromRoute('simple_oauth_fallback_header_test.protected_route');
    $auth_response = $this->assertValidTokenResponse($this->post($this->url, [
      'grant_type' => 'password',
      'client_id' => $this->client->uuid(),
      'client_secret' => $this->clientSecret,
      'username' => $this->user->getAccountName(),
      'password' => $this->user->pass_raw,
      'scope' => $this->scope,
    ]), TRUE);

    $assert = function (int $status_code, array $options, array $settings = NULL) use ($protected_route): void {
      // Do not throw exceptions on non `2xx`.
      $options['http_errors'] = FALSE;

      if ($settings !== NULL) {
        foreach ($settings as $key => $value) {
          $settings[$key] = (object) [
            'value' => $value,
            'required' => TRUE,
          ];
        }

        $this->writeSettings(['settings' => $settings]);
      }

      static::assertSame($status_code, $this->get($protected_route, $options)->getStatusCode());
    };

    // Without authorization the route must not be accessible.
    $assert(403, []);
    // The use of a GET query is not allowed by default.
    $assert(403, ['query' => ['access_token' => $auth_response['access_token']]]);
    $assert(200, ['query' => ['access_token' => $auth_response['access_token']]], ['simple_oauth_allow_get_query' => TRUE]);
    $assert(200, ['headers' => ['Authorization' => 'Bearer ' . $auth_response['access_token']]]);
    $assert(401, ['headers' => ['X-OAuth-Authorization' => 'Bearer']]);
    $assert(200, ['headers' => ['X-OAuth-Authorization' => 'Bearer ' . $auth_response['access_token']]]);
    $assert(200, ['headers' => ['X-Custom-Authorization' => 'Bearer ' . $auth_response['access_token']]], ['simple_oauth_fallback_header' => 'X-Custom-Authorization']);
  }

}
