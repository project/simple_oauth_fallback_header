<?php

namespace Drupal\simple_oauth_fallback_header_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * The controller for testing.
 */
class SimpleOauthFallbackHeaderTestController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function protectedRoute(): JsonResponse {
    return new JsonResponse();
  }

}
