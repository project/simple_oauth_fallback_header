<?php

namespace Drupal\simple_oauth_fallback_header;

use Drupal\Core\Site\Settings;
use Drupal\simple_oauth\PageCache\DisallowSimpleOauthRequests as DisallowSimpleOauthRequestsBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * {@inheritdoc}
 *
 * This implementation allows passing an access token in the `access_token`
 * GET parameter or within the `X-OAuth-Authorization` HTTP header.
 *
 * You may consider tuning the following configuration via `settings.php`:
 * @code
 * $settings['simple_oauth_fallback_header'] = 'X-My-Api-Auth';
 * $settings['simple_oauth_allow_get_query'] = TRUE;
 * @endcode
 */
class DisallowSimpleOauthRequests extends DisallowSimpleOauthRequestsBase {

  /**
   * {@inheritdoc}
   */
  public function isOauth2Request(Request $request): bool {
    // Support alternative header and the `access_token` GET parameter.
    if ($access_token = $this->getAccessToken($request)) {
      $request->headers->set('Authorization', 'Bearer ' . \str_replace('Bearer ', '', $access_token));
    }

    return parent::isOauth2Request($request);
  }

  /**
   * Returns OAuth access token extracted from custom header or GET query.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The inbound request.
   *
   * @return string|null
   *   The access token if available.
   */
  protected function getAccessToken(Request $request): ?string {
    $access_token = $request->headers->get(Settings::get('simple_oauth_fallback_header', 'X-OAuth-Authorization'));

    if (empty($access_token) && Settings::get('simple_oauth_allow_get_query', FALSE)) {
      $access_token = $request->query->get('access_token');
    }

    return $access_token;
  }

}
